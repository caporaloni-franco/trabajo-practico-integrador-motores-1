using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string Escena1 = "Escena1";
  



    // Método llamado cuando se presiona el botón.
    public void CargarEscenaNueva()
    {
        GestorDeAudio.instancia.PausarSonido("Musica");
        SceneManager.LoadScene("Escena1");
        
    }
}

