using UnityEngine;
using System.Collections;

public class MovimientoVerticalEnemigo : MonoBehaviour
{
    public float alturaMaxima = 5.0f; // Altura máxima a la que se moverá el objeto
    public float velocidad = 2.0f; // Velocidad de movimiento
    public int vecesAMoverse = 3; // Número de veces que el objeto se moverá de arriba a abajo

    private Vector3 posicionInicial;
    private bool moviendoseArriba = true;
    private int vecesMovidas = 0;

    private void Start()
    {
        posicionInicial = transform.position;
        StartCoroutine(MoverArribaAbajo());
    }

    private IEnumerator MoverArribaAbajo()
    {
        while (vecesMovidas < vecesAMoverse)
        {
            float objetivoY = moviendoseArriba ? (posicionInicial.y + alturaMaxima) : posicionInicial.y;

            while (Mathf.Abs(transform.position.y - objetivoY) > 0.01f)
            {
                float step = velocidad * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, objetivoY, transform.position.z), step);
                yield return null;
            }

            moviendoseArriba = !moviendoseArriba;
            vecesMovidas++;

            yield return new WaitForSeconds(0.5f); // Espera 0.5 segundos antes de invertir la dirección
        }
    }
}

