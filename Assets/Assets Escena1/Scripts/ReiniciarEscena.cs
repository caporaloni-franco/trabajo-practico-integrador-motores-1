using UnityEngine;
using UnityEngine.SceneManagement;

public class ReiniciarEscena : MonoBehaviour
{
    // Este método se llama cuando otro collider entra en contacto con el collider del objeto actual.
    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el otro collider pertenece al jugador.
        if (other.CompareTag("Player"))
        {
            // Reinicia la escena actual.
            ReiniciarEscenaActual();
        }
    }

    // Método para reiniciar la escena actual.
    private void ReiniciarEscenaActual()
    {
        // Obtiene el índice de la escena actual.
        int indiceEscenaActual = SceneManager.GetActiveScene().buildIndex;

        // Carga la misma escena por su índice.
        SceneManager.LoadScene(indiceEscenaActual);
    }
}
