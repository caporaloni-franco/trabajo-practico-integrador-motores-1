using UnityEngine;

public class ControladorJugador : MonoBehaviour
{
    public float velocidadMovimiento = 5f;
    public float fuerzaSalto = 10f;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    void Update()
    {
        MoverJugador();
        Saltar();

        // Reproduce el sonido de salto cuando se presiona la tecla de espacio
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GestorDeAudio.instancia.ReproducirSonido("Salto");
        }
    }

    void MoverJugador()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, 0f) * velocidadMovimiento * Time.deltaTime;
        transform.Translate(movimiento);
    }

    void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Mathf.Abs(rb.velocity.y) < 0.001f)
            {
                rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
            }
        }
    }
}