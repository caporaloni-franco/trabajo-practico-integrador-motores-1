using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helices : MonoBehaviour
{
public float velocidadRotacion = 200f; // Velocidad de rotación en grados por segundo

    void Update()
    {
        // Rotar el rectángulo como una hélice
        transform.Rotate(Vector3.up, velocidadRotacion * Time.deltaTime);
    }
}
