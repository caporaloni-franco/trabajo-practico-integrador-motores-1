using UnityEngine;

public class Viento : MonoBehaviour
{
    public Vector3 direccionDelViento = Vector3.right; // Ajusta la dirección del "viento".
    public float fuerzaDelViento = 10f; // Ajusta la fuerza del "viento".

    void OnTriggerEnter(Collider other)
    {
        // Verifica si el otro collider tiene el tag "Player".
        if (other.CompareTag("Player"))
        {
            // Obtén el componente Rigidbody del jugador.
            Rigidbody rb = other.GetComponent<Rigidbody>();

            // Aplica una fuerza constante en la dirección del "viento".
            if (rb != null)
            {
                // Normaliza la dirección y aplica la fuerza.
                direccionDelViento.Normalize();
                rb.AddForce(direccionDelViento * fuerzaDelViento, ForceMode.Impulse);
            }
        }
    }
}
