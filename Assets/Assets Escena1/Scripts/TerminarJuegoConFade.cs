using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TerminarJuegoConFade : MonoBehaviour
{
    public float duracionFade = 2f;  // Duración en segundos del efecto de fade
    public Image imagenFade;         // Imagen negra para el fade
    public Text textoContinuara;     // Texto "Continuará"

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Inicia la corrutina para el efecto fade.
            StartCoroutine(FadeOutAndShowText());
        }
    }

    IEnumerator FadeOutAndShowText()
    {
        float tiempoInicio = Time.time;

        // Mientras el tiempo de fade no haya alcanzado la duración deseada.
        while (Time.time < tiempoInicio + duracionFade)
        {
            float alpha = Mathf.Lerp(0f, 1f, (Time.time - tiempoInicio) / duracionFade);
            imagenFade.color = new Color(0f, 0f, 0f, alpha);

            yield return null;
        }

        // Asegúrate de que la imagen sea completamente opaca al final del fade.
        imagenFade.color = new Color(0f, 0f, 0f, 1f);

        // Activa el texto "Continuará".
        textoContinuara.gameObject.SetActive(true);

        // Espera unos segundos antes de cargar la siguiente escena (o realiza alguna otra acción).
        yield return new WaitForSeconds(2f);

        // Puedes cambiar el nombre de la escena o cargar la siguiente escena según tu lógica.
        SceneManager.LoadScene("Escena2");
        SceneManager.UnloadScene("Escena1");
    }
}
