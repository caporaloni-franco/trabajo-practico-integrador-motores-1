using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public Transform objetivo; // El objeto que la cámara seguirá (jugador en tercera persona)
    public float velocidadRotacion = 3.0f; // Velocidad de rotación de la cámara
    public float distancia = 5.0f; // Distancia entre la cámara y el jugador
    public float altura = 2.0f; // Altura de la cámara sobre el jugador

    private void LateUpdate()
    {
        // Rotación de la cámara con el movimiento del ratón
        float rotacionHorizontal = Input.GetAxis("Mouse X") * velocidadRotacion;
        objetivo.Rotate(Vector3.up, rotacionHorizontal);

        // Configura la posición de la cámara en relación con el jugador
        transform.position = objetivo.position - objetivo.forward * distancia;
        transform.position = new Vector3(transform.position.x, objetivo.position.y + altura, transform.position.z);

        // Mira hacia el jugador
        transform.LookAt(objetivo.position);
    }
}
