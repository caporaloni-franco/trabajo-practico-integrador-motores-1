using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader2 : MonoBehaviour
{
    public string sceneToLoadInBackground;

    void Start()
    {
        // Iniciar la carga de la escena en segundo plano
        LoadSceneInBackground();
    }

    void LoadSceneInBackground()
    {
        // Cargar la escena en segundo plano de manera asíncrona
        SceneManager.LoadSceneAsync(sceneToLoadInBackground, LoadSceneMode.Additive);
    }
}
