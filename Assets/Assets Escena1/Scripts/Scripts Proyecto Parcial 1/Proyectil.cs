using UnityEngine;

public class Proyectil : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Suelo"))
        {
            // El proyectil ha colisionado con un objeto con el tag "Piso"
            // Destruir el proyectil
            Destroy(gameObject);
        }
    }
}
