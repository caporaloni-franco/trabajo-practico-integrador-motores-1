using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CuentaRegresiva : MonoBehaviour
{
    public TextMeshProUGUI textoCuentaRegresiva;
    private float cuentaReg = 90.0f;

    void Update()
    {
        cuentaReg -= Time.deltaTime;
        textoCuentaRegresiva.text = Mathf.Round(cuentaReg).ToString();

        if (cuentaReg <= 0)
        {
            SceneManager.LoadScene("EscenaEnd");
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarTiempo();
        }
    }

    private void ReiniciarTiempo()
    {
        cuentaReg = 60.0f;
    }
}