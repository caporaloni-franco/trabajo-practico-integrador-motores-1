using UnityEngine;

public class ControlSalto : MonoBehaviour
{
    public float fuerzaSalto = 6.0f;
    public int maxSaltos = 2;
    public LayerMask capaSuelo;
    public Transform pies;

    private int saltosRestantes;
    private bool enSuelo;

    void Start()
    {
        saltosRestantes = maxSaltos;
    }

    void Update()
    {
        enSuelo = Physics.CheckSphere(pies.position, 0.1f, capaSuelo);

        if (enSuelo && GetComponent<Rigidbody>().velocity.y < 0)
        {
            saltosRestantes = maxSaltos;
        }

        if (Input.GetKeyDown(KeyCode.Space) && saltosRestantes > 0)
        {
            GetComponent<Rigidbody>().velocity = Vector3.up * fuerzaSalto;
            saltosRestantes--;
        }
    }
}