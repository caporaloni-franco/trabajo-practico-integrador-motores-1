
using UnityEngine;
using UnityEngine.SceneManagement;

public class PerseguirJugador : MonoBehaviour
{
    public Transform jugador;  // Referencia al objeto del jugador
    public float velocidad = 3.0f;  // Velocidad de persecución
    public float distanciaAtrape = 1.0f;  // Distancia a la que el objeto atrapa al jugador


    private void Update()
    {
        // Comprueba si el jugador está lo suficientemente cerca para activar la persecución
        Vector3 direccion = jugador.position - transform.position;
        direccion.y = 0;  // Ignora la diferencia de altura

        
        
        
            // Calcula la dirección hacia el jugador
            direccion = jugador.position - transform.position;
            direccion.y = 0;  // Ignora la diferencia de altura

            // Comprueba si el jugador está lo suficientemente cerca para atraparlo
            if (direccion.magnitude <= distanciaAtrape)
            {
                // Reinicia la escena cuando el jugador es atrapado
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                // Persigue al jugador
                direccion.Normalize();
                transform.Translate(direccion * velocidad * Time.deltaTime);
            }
        }
    }