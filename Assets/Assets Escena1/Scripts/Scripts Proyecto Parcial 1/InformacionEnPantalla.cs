using UnityEngine;
using UnityEngine.UI;

public class InformacionEnPantalla : MonoBehaviour
{
    public Text textoInformacion; 
    public float intervaloActualizacion = 50.0f; 
    private float tiempoUltimaActualizacion;
    private string[] mensajes = new string[]
    {
        "¡Cuidado con el enemigo azul!",
        "¡Sigue avanzando!",
        "¡No te rindas!",
        "¡Ya falta poco!",
    };
    private int indiceMensajeActual = 0;

    void Start()
    {
       
        tiempoUltimaActualizacion = Time.time;
    }

    void Update()
    {
        
        if (Time.time - tiempoUltimaActualizacion >= intervaloActualizacion)
        {
            
            textoInformacion.text = mensajes[indiceMensajeActual];

           
            indiceMensajeActual = (indiceMensajeActual + 1) % mensajes.Length;

           
            tiempoUltimaActualizacion = Time.time;
        }
    }
}
