using UnityEngine;

public class SeguimientoCamara : MonoBehaviour
{
    public Transform objetivo; // El objeto que la cámara seguirá
    public float distancia = 5.0f; // Distancia desde el objetivo
    public float altura = 2.0f; // Altura de la cámara
    public float velocidadRotacion = 2.0f; // Velocidad de rotación de la cámara

    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private const float minY = -89.0f;
    private const float maxY = 89.0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        currentX += Input.GetAxis("Mouse X") * velocidadRotacion;
        currentY -= Input.GetAxis("Mouse Y") * velocidadRotacion;
        currentY = Mathf.Clamp(currentY, minY, maxY);
    }

    void LateUpdate()
    {
        Vector3 direction = new Vector3(0, 0, -distancia);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        transform.position = objetivo.position + rotation * direction;
        transform.LookAt(objetivo.position);
        transform.Translate(Vector3.up * altura);
    }
}
