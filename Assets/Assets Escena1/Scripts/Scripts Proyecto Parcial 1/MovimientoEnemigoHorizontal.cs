using UnityEngine;

public class MovimientoEnemigoHorizontal : MonoBehaviour
{
    public float velocidad = 2.0f; // Velocidad de movimiento del enemigo
    public float distanciaLimite = 5.0f; // Distancia máxima que el enemigo puede moverse

    private Vector3 puntoInicial;
    private bool moviendoseHaciaDerecha = true;

    void Start()
    {
        puntoInicial = transform.position;
    }

    void Update()
    {
        if (moviendoseHaciaDerecha)
        {
            transform.Translate(Vector3.right * velocidad * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * velocidad * Time.deltaTime);
        }

        // Comprueba si el enemigo ha alcanzado un límite
        float distanciaRecorrida = Vector3.Distance(puntoInicial, transform.position);
        if (distanciaRecorrida >= distanciaLimite)
        {
            // Invierte la dirección
            moviendoseHaciaDerecha = !moviendoseHaciaDerecha;
        }
    }
}
