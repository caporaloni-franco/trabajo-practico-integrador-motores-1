using UnityEngine;
using System.Collections;

public class ConsumiblePowerUp : MonoBehaviour
{
    public float aumentoTamaño = 2.0f; 
    public float aumentoVelocidad = 2.0f; 
    public float aumentoSalto = 2.0f; 
    public float duracionPowerUp = 10.0f; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            AumentarTamañoVelocidadSalto(other.gameObject);

           
            gameObject.SetActive(false);

        }
    }

    private void AumentarTamañoVelocidadSalto(GameObject jugador)
    {
        
        jugador.transform.localScale *= aumentoTamaño;

        
        MovimientoObjeto movimientoJugador = jugador.GetComponent<MovimientoObjeto>();
        if (movimientoJugador != null)
        {
            movimientoJugador.AumentarVelocidad(aumentoVelocidad);
        }

        
        if (movimientoJugador != null)
        {
            movimientoJugador.AumentarSalto(aumentoSalto);
        }
    }
}
