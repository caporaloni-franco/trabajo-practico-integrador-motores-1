using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TocarPelotaRosa : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.CompareTag("Player"))
        {
            Ganar();
        }
    }

    void Ganar()
    {
        SceneManager.LoadScene("Ganaste");
    }

    void Perder()
    {
        SceneManager.LoadScene("Perdiste");
    }
}
