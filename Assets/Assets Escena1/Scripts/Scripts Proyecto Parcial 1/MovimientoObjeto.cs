using UnityEngine;

public class MovimientoObjeto : MonoBehaviour
{
    public float velocidad = 5.0f;
    public float fuerzaSalto = 5.0f; 
    private bool enSuelo = true; 
    private Transform camara; 

    private float fuerzaSaltoOriginal; 

    void Start()
    {
        
        fuerzaSaltoOriginal = fuerzaSalto;

        
        camara = Camera.main.transform;
    }

    void Update()
    {
       
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        
        Vector3 desplazamiento = Vector3.zero;

        if (camara != null)
        {
            
            Vector3 direccionCamara = camara.forward;
            direccionCamara.y = 0; 

            
            desplazamiento = (direccionCamara * movimientoVertical + camara.right * movimientoHorizontal).normalized * velocidad * Time.deltaTime;
        }

        
        transform.Translate(desplazamiento);

        
        if (Input.GetButtonDown("Jump") && enSuelo)
        {
            Saltar();
        }
    }

    void Saltar()
    {
        
        GetComponent<Rigidbody>().AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        enSuelo = false; 
    }

    public void AumentarSalto(float aumento)
    {
        
        fuerzaSalto *= aumento;
    }

    public void RestaurarSalto()
    {
        
        fuerzaSalto = fuerzaSaltoOriginal;
    }

    public void AumentarVelocidad(float aumento)
    {
        
        velocidad *= aumento;
    }

    public void RestaurarVelocidad()
    {
        
        velocidad = 5.0f; 
    }

   
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Suelo"))
        {
            enSuelo = true; 
        }
    }
}
