using UnityEngine;
using UnityEngine.SceneManagement;

public class ReiniciarJuego : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Reiniciar la escena actual
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Escape))

        {

            SceneManager.UnloadScene(SceneManager.GetActiveScene().name);

        }


    }

  private void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto tiene el tag "Enemigo" o "Bala"
        if (collision.gameObject.CompareTag("Enemigo") || collision.gameObject.CompareTag("bala"))
        {
            // Reiniciar la escena
            RestartScene();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto tiene el tag "Enemigo" o "Bala"
        if (other.CompareTag("Enemigo") || other.CompareTag("bala"))
        {
            // Reiniciar la escena
            RestartScene();
        }
    }

    private void RestartScene()
    {
        // Puedes ajustar esto según el nombre de tu escena actual
        string currentSceneName = SceneManager.GetActiveScene().name;

        // Reiniciar la escena actual
        SceneManager.LoadScene(currentSceneName);
    }
}

