using UnityEngine;

public class RespawnJugador : MonoBehaviour
{
    public GameObject jugador;           // El GameObject del jugador que será respawnado
    public Transform puntoDeRespawn;     // El punto donde el jugador reaparecerá

    private Vector3 posicionInicial;    // Guarda la posición inicial del jugador al inicio

    private void Start()
    {
        // Guarda la posición inicial del jugador
        posicionInicial = jugador.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemigo")) // Verifica si el jugador ha activado el trigger de un enemigo
        {
            // Desactiva el jugador
            jugador.SetActive(false);

            // Respawnea al jugador al punto de respawn después de un breve retraso (opcional)
            Invoke("RespawnearJugador", 2f);
        }
    }

    private void RespawnearJugador()
    {
        // Activa el jugador
        jugador.SetActive(true);

        // Coloca al jugador en el punto de respawn
        jugador.transform.position = puntoDeRespawn.position;
    }
}
