using UnityEngine;

public class SeguirJugador : MonoBehaviour
{
    public Transform jugador;
    public float suavizado = 5f;

    void FixedUpdate()
    {
        SeguirJugadorEnAmbosEjes();
    }

    void SeguirJugadorEnAmbosEjes()
    {
        Vector3 nuevaPosicion = new Vector3(jugador.position.x, jugador.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, nuevaPosicion, suavizado * Time.deltaTime);
    }
}
