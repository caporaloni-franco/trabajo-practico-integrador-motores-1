using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarEscenaConUnload : MonoBehaviour
{
    public string nombreDeEscenaACargar = "Escena2"; // Nombre de la escena a cargar.

    // Este método se llama cuando otro collider entra en contacto con el collider del objeto actual.
    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el otro collider tiene el tag "Player".
        if (other.CompareTag("Player"))
        {
            // Carga la nueva escena y descarga la escena actual.
            RealizarCambioDeEscena();
        }
    }

    // Método para cambiar de escena y descargar la escena actual.
    private void RealizarCambioDeEscena()
    {
        // Descarga la escena actual.
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

        // Carga la nueva escena.
        SceneManager.LoadScene(nombreDeEscenaACargar);
    }
}
