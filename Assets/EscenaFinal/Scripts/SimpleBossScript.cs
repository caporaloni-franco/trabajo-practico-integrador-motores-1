using UnityEngine;

public class SimpleBossScript : MonoBehaviour
{
    public int maxHealth = 100;
    private int currentHealth;

    public float attackCooldown = 2f; // Tiempo de espera entre ataques
    private float nextAttackTime = 0f;

    public GameObject attackPrefab1; // Prefab del primer ataque
    public GameObject attackPrefab2; // Prefab del segundo ataque
    public GameObject attackPrefab3; // Prefab del tercer ataque

    private Transform player;

    public float movementSpeed = 5f; // Velocidad de movimiento del jefe

    // Tiempo que el jefe espera antes de cambiar de dirección
    public float changeDirectionCooldown = 5f;
    private float nextDirectionChangeTime = 0f;

    private void Start()
    {
        currentHealth = maxHealth;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        MoveTowardsPlayer();

        // Implementar lógica de ataque aquí
        if (Time.time >= nextAttackTime)
        {
            // Lógica para elegir uno de los tres ataques aleatoriamente
            int randomAttack = Random.Range(1, 4);
            PerformAttack(randomAttack);

            // Establecer el próximo tiempo de ataque
            nextAttackTime = Time.time + attackCooldown;
        }

        // Cambiar de dirección aleatoria después de cierto tiempo
        if (Time.time >= nextDirectionChangeTime)
        {
            ChangeDirectionRandomly();

            // Establecer el próximo tiempo de cambio de dirección
            nextDirectionChangeTime = Time.time + changeDirectionCooldown;
        }
    }

    private void MoveTowardsPlayer()
    {
        // Moverse hacia el jugador
        transform.LookAt(player);
        transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
    }

    private void ChangeDirectionRandomly()
    {
        // Cambiar de dirección aleatoria
        float randomAngle = Random.Range(0f, 360f);
        transform.rotation = Quaternion.Euler(0f, randomAngle, 0f);
    }

    private void PerformAttack(int attackNumber)
    {
        switch (attackNumber)
        {
            case 1:
                // Lógica del primer ataque
                LaunchProjectile(attackPrefab1);
                break;

            case 2:
                // Lógica del segundo ataque
                LaunchProjectile(attackPrefab2);
                break;

            case 3:
                // Lógica del tercer ataque
                LaunchProjectile(attackPrefab3);
                break;

            default:
                Debug.LogError("Número de ataque no reconocido: " + attackNumber);
                break;
        }
    }

 private void LaunchProjectile(GameObject projectilePrefab)
{
    // Instanciar el proyectil y apuntarlo hacia el jugador
    GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
    
    // Calcular la dirección hacia el jugador
    Vector3 directionToPlayer = (player.position - transform.position).normalized;

    // Obtener la rotación para apuntar hacia la dirección del jugador
    Quaternion lookRotation = Quaternion.LookRotation(directionToPlayer);

    // Aplicar la rotación al proyectil
    projectile.transform.rotation = lookRotation;

    // Puedes ajustar la velocidad del proyectil según tus necesidades
    float projectileSpeed = 10f;
    projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * projectileSpeed;

    float projectileLifetime = 5f;
     Destroy(projectile, projectileLifetime);
}



    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        // Verificar si el jefe ha sido derrotado
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        // Lógica de muerte del jefe
        Debug.Log("Boss defeated!");
        Destroy(gameObject); // Puedes personalizar esto según tus necesidades
    }
}
