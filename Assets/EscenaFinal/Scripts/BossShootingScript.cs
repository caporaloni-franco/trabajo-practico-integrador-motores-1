using UnityEngine;

public class BossShootingScript : MonoBehaviour
{
    public GameObject proyectilPrefab; // Asigna el prefab del proyectil desde el editor
    public float shootingInterval = 1.5f; // Intervalo de tiempo entre disparos
    public int numberOfProjectiles = 8; // Número de proyectiles a disparar

    private float nextShootingTime;

    private void Update()
    {
        // Verifica si es el momento de disparar
        if (Time.time >= nextShootingTime)
        {
            ShootProjectiles();

            // Establece el próximo momento de disparo
            nextShootingTime = Time.time + shootingInterval;
        }
    }

    private void ShootProjectiles()
    {
        float angleStep = 360f / numberOfProjectiles;

        // Disparar proyectiles en todas las direcciones horizontales
        for (int i = 0; i < numberOfProjectiles; i++)
        {
            float angle = i * angleStep;
            Quaternion rotation = Quaternion.Euler(0f, angle, 0f);

            // Instanciar el proyectil y asignarle la rotación
            GameObject projectile = Instantiate(proyectilPrefab, transform.position, rotation);
            
            // Puedes ajustar la velocidad del proyectil según tus necesidades
            float projectileSpeed = 10f;
            projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * projectileSpeed;

            // Ajusta el tiempo de vida del proyectil según tus necesidades
            float projectileLifetime = 5f;
            Destroy(projectile, projectileLifetime);
        }
    }
}
