using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderScript : MonoBehaviour
{
    void Update()
    {
        // Verificar si la tecla "R" está siendo presionada
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Cargar la escena "Escena1"
            LoadScene("Escena1");
        }
    }

    void LoadScene(string sceneName)
    {
        // Verificar si la escena existe antes de cargarla
        if (SceneManager.GetSceneByName(sceneName) != null)
        {
            SceneManager.LoadScene(sceneName);
        }
        else
        {
            Debug.LogError("La escena " + sceneName + " no existe.");
        }
    }
}
