using UnityEngine;

public class FireballScript : MonoBehaviour
{
    public int damage = 20;
    public float speed = 10f;

    private void Start()
    {
        // Aplicar una velocidad hacia adelante para simular el movimiento de la bola de fuego
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            
            
        }

        if (other.CompareTag("Pared")) 
        {
        Destroy(gameObject);
        }

        if (other.CompareTag("Suelo")) 
        {
        Destroy(gameObject);
        }


    }
}
