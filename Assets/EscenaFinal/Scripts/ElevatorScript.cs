using UnityEngine;

public class ElevatorScript : MonoBehaviour
{
    public float elevationHeight = 5f;        // Altura a la que se eleva el objeto
    public float elevationSpeed = 2f;         // Velocidad de elevación
    public float waitTime = 2f;               // Tiempo de espera en la posición superior
    public float descentSpeed = 1f;           // Velocidad de descenso

    private Vector3 initialPosition;
    private bool isMovingUp = true;

    void Start()
    {
        initialPosition = transform.position;
        InvokeRepeating("ToggleMovementDirection", 0f, waitTime);
    }

    void Update()
    {
        if (isMovingUp)
        {
            MoveUp();
        }
        else
        {
            MoveDown();
        }
    }

    void MoveUp()
    {
        float step = elevationSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, initialPosition + new Vector3(0, elevationHeight, 0), step);
    }

    void MoveDown()
    {
        float step = descentSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, initialPosition, step);
    }

    void ToggleMovementDirection()
    {
        isMovingUp = !isMovingUp;
    }
}
