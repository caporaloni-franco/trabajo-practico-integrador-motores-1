using UnityEngine;

public class MoverIzquierda : MonoBehaviour
{
    public float distanciaMaxima = 5f;
    public float velocidad = 2f;

    private Vector3 posicionInicial;

    void Start()
    {
        posicionInicial = transform.position;
    }

    void Update()
    {
        MoverObjetoAdelante();
    }

    void MoverObjetoAdelante()
    {
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);

        if (Vector3.Distance(posicionInicial, transform.position) >= distanciaMaxima)
        {
            // Vuelve a la posición inicial
            transform.position = posicionInicial;
        }
    }
}
