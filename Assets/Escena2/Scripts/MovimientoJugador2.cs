using UnityEngine;

public class MovimientoJugador2 : MonoBehaviour
{
    public float velocidadMovimiento = 5f;
    public float fuerzaSaltoNormal = 10f;

    private Rigidbody rb;
    private float fuerzaSaltoActual; // Nueva variable para almacenar la fuerza actual de salto

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        fuerzaSaltoActual = fuerzaSaltoNormal; // Inicializa la fuerza de salto con el valor normal
        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    void Update()
    {
        MoverJugador();
        Saltar();

        // Reproduce el sonido de salto cuando se presiona la tecla de espacio
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GestorDeAudio.instancia.ReproducirSonido("Salto");
        }
    }

    void MoverJugador()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidadMovimiento * Time.deltaTime;
        transform.Translate(movimiento);
    }

    void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Mathf.Abs(rb.velocity.y) < 0.001f)
            {
                rb.AddForce(Vector3.up * fuerzaSaltoActual, ForceMode.Impulse);
            }
        }
    }

    // Nuevo método para ajustar la fuerza de salto desde el PowerUp
    public void AjustarFuerzaSalto(float nuevaFuerza)
    {
        fuerzaSaltoActual = nuevaFuerza;
    }
}
