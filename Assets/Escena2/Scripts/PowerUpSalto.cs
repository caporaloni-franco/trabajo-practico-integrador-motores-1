using UnityEngine;

public class PowerUpSalto : MonoBehaviour
{
    public float fuerzaSaltoPotenciada = 15f; // Fuerza de salto cuando el power-up está activo
    public float duracionPowerUp = 10f; // Duración del power-up en segundos

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el otro collider tiene el tag "Player".
        if (other.CompareTag("Player"))
        {
            // Obtén el componente MovimientoJugador2 del jugador.
            MovimientoJugador2 movimientoJugador = other.GetComponent<MovimientoJugador2>();
            if (movimientoJugador != null)
            {
                // Activa el power-up y ajusta la fuerza de salto.
                movimientoJugador.AjustarFuerzaSalto(fuerzaSaltoPotenciada);
                // Comienza la cuenta regresiva para desactivar el power-up.
                Invoke("DesactivarPowerUp", duracionPowerUp);
            }

            // Desactiva el objeto del power-up (puedes desactivarlo o destruirlo según tus necesidades).
            gameObject.SetActive(false);
        }
    }

    private void DesactivarPowerUp()
    {
        // Obtén el componente MovimientoJugador2 del jugador.
        MovimientoJugador2 movimientoJugador = GameObject.FindGameObjectWithTag("Player").GetComponent<MovimientoJugador2>();
        if (movimientoJugador != null)
        {
            // Desactiva el power-up y restaura la fuerza de salto normal.
            movimientoJugador.AjustarFuerzaSalto(movimientoJugador.fuerzaSaltoNormal);
        }
    }
}
