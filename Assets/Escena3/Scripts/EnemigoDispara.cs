using UnityEngine;
public class EnemigoDispara : MonoBehaviour
{
    public float velocidadDisparo = 5.0f;
    public GameObject jugador;
    public float distanciaMinDisparo = 10.0f;
    public float distanciaMinEliminacion = 2.0f;
    public GameObject proyectilPrefab;
    public float frecuenciaDisparo = 1.0f; // Frecuencia de disparo en segundos

    private bool puedeDisparar = false;
    private bool puedeEliminar = false;
    private float tiempoUltimoDisparo = 0.0f;

    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    void Update()
    {
        float distanciaJugador = Vector3.Distance(transform.position, jugador.transform.position);

        if (distanciaJugador <= distanciaMinDisparo)
        {
            puedeDisparar = true;
        }

        if (distanciaJugador <= distanciaMinEliminacion)
        {
            puedeEliminar = true;
        }

        if (puedeDisparar)
        {
            Disparar();
        }

        if (puedeEliminar)
        {
            EliminarEnemigo();
        }
    }

    void Disparar()
    {
        if (Time.time - tiempoUltimoDisparo >= frecuenciaDisparo)
        {
            Vector3 direccionDisparo = (jugador.transform.position - transform.position).normalized;
            GameObject proyectil = Instantiate(proyectilPrefab, transform.position, Quaternion.identity);
            proyectil.GetComponent<Rigidbody>().velocity = direccionDisparo * velocidadDisparo;
            tiempoUltimoDisparo = Time.time;
        }
    }

    void EliminarEnemigo()
    {
        Destroy(gameObject);
    }
}

