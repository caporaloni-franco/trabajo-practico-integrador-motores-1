using UnityEngine;

public class Bala : MonoBehaviour
{
    public float velocidad = 20.0f;

    void Update()
    {
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
    }
}
public class Bala1 : MonoBehaviour
{
    public int damage = 10; 
}