using UnityEngine;

public class ControlObjeto : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;

    void Start()
    {
        hp = 500;
        jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
       
    }

    private void recibirDaño()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Disparo"))
        {
            recibirDaño();
            // Destruye la bala al colisionar con el objeto
            Destroy(collision.gameObject);
        }
    }
}
