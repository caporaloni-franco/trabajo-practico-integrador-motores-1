using UnityEngine;

public class EnemigoDisparador : MonoBehaviour
{
    public Transform jugador;  // Asigna el objeto del jugador en el Inspector
    public GameObject balaPrefab;  // Asigna el prefab de la bala en el Inspector
    public Transform puntoDisparo;  // Punto de origen del disparo

    public float velocidadDisparo = 5f;
    public float tiempoEsperaEntreDisparos = 1f;

    private float tiempoSiguienteDisparo = 0f;

    void Update()
    {
        // Apunta al jugador
        transform.LookAt(jugador);

        // Dispara si ha pasado el tiempo de espera
        if (Time.time >= tiempoSiguienteDisparo)
        {
            Disparar();
            tiempoSiguienteDisparo = Time.time + 1f / tiempoEsperaEntreDisparos;
        }
    }

    void Disparar()
    {
        // Crea una instancia de la bala en el punto de disparo
        GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, puntoDisparo.rotation);

        // Aplica velocidad a la bala
        Rigidbody rb = bala.GetComponent<Rigidbody>();
        rb.AddForce(puntoDisparo.forward * velocidadDisparo, ForceMode.Impulse);
    }
}
