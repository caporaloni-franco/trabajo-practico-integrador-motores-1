using UnityEngine;

public class PowerUp2 : MonoBehaviour
{
    public float reduccionDeTamaño = 0.5f; // Factor de reducción de tamaño
    public float aumentoDeSalto = 2f; // Factor de aumento de salto
    public float duracionDelPowerUp = 10f; // Duración del efecto del power-up en segundos

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Aplica el efecto del power-up al jugador
            AplicarEfecto(other.gameObject);

            // Desactiva el power-up
            gameObject.SetActive(false);

            // Puedes agregar aquí lógica adicional, como reproducir un sonido, efectos visuales, etc.

            // Programa la reactivación del power-up después de cierto tiempo
            Invoke("ReactivatePowerUp", duracionDelPowerUp);
        }
    }

    private void AplicarEfecto(GameObject jugador)
    {
        // Reduce el tamaño del jugador
        jugador.transform.localScale *= reduccionDeTamaño;

        // Aumenta la fuerza del salto del jugador aplicando una fuerza vertical
        Rigidbody rb = jugador.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.AddForce(Vector3.up * Mathf.Sqrt(2 * aumentoDeSalto * Mathf.Abs(Physics.gravity.y)), ForceMode.Impulse);
        }
    }

    private void ReactivatePowerUp()
    {
        // Reactiva el power-up después de cierto tiempo
        gameObject.SetActive(true);

        // Puedes reiniciar cualquier estado adicional del power-up aquí
    }
}
