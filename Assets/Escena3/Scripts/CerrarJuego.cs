using UnityEngine;

public class CerrarJuego : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // El jugador ha tocado el trigger, cierra el juego
            CerrarAplicacion();
        }
    }

    void CerrarAplicacion()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
